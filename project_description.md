# Settings Guessing with Machine Learning

## Terms and definitions

* **Bears**: Code-analysis modules of coala.  

* **Setting**: A key-value pair used by bears for custom functioning.  

* **Result**: A result is any issue or potential problem that an analysis routine (a *bear*) has detected with the code.

* **Style features**: This is a kind of intermediate data. The SFM processes sample files and makes 'style features'. These 'style features' are used as input to the Machine Learning algorithm. The 'style features' are also converted into 'settings' for the bears.  

* **Style Feature Maker(SFM)**: The module that converts sample files into 'style features'.

* **Machine Learner(ML)**: The module that consists of Machine Learning algorithm(s).  

* **Feature to Settings Translator(FST)**: The module that converts 'style features' into 'settings'.

## TL;DR

The aim of the project is to create a functionality by which it is possible to 'learn' the preferred coding-style of the user and to translate the learnt data into something that coala, which is a static code-analysis framework, understands.  
This will be done using 'Settings Guessing with Machine Learning'(SGML). SGML is divided into 3 modules - 'Style Feature Maker' (SFM), 'Machine Learner' (ML), and 'Feature to Settings Translator' (FST).  

The SFM will analyze sample files from which it'll make 'style features'. The ML will take the 'style features' as input and learn about the user and use the learnt info to improve further making of 'style features'. The FST will simply translate the style checkpoints into parameters which can be directly used by required bears.

## Detailed description

coala has code-analysis modules called *bears*, most of which are built on top of linting and analysis tools like *pylint* or *eslint*. These *bears*, by default, use the default settings of the underlying tools, but can pass on custom parameters by means of command-line arguments or by editing the coala configuration file(called *.coafile*).  

One problem with the above workflow is that the user would have to be somewhat familiar with the underlying tool and its parameters. Another problem is that it could be a bit cumbersome, listing down all the custom settings.  
Settings Guessing with emphasis on Machine Learning (SGML) can solve both these problems. The first problem will be solved as SGML will automatically take care of translating real code-styles into parameters internally (the user won't need to know about these anymore). For the second problem, SGML will make it very effortless for the user to have custom settings as, all the user has to do is run SGML on a bunch of sample files and the rest will be taken care of.  

Below is the design architecture of SGML. It is divided into 3 main modules – 'Style Feature Maker' (SFM), 'Machine Learner' (ML), and 'Feature to Settings Translator' (FST).

![SGML architecture](img/sgml_architecture.png)

SFM analyzes the sample files and extract out 'style features'. These 'style features' correspond various style elements like 'tab-width', 'class docstring regex', 'variable-name regex' etc. This extraction can be done in one of two ways:  

 1. Run the required bears on the sample files. Note all the warnings/errors that the bears point out. Assuming that the sample files are correct according to the user's code style, add style checkpoints based on the noted warnings/errors such that when translated to parameters, adapt the bears to the user's code style. This will make it so that all future code written is guided by the code style as in the sample files.

 2. Have a predetermined list of style checkpoints for each language and determine what checkpoints come under which bears. Scan through all the sample files and assign values to the checkpoints using a custom parser.

The second way will require a custom parser to be built for every language and therefore, the first method is preferred as of now.
    
The ML will take the style checkpoints as an input and learn about the user. From previously learnt information, it can add more style checkpoints or modify existing checkpoints. It serves as a sort of a feedback mechanism.
    
The FST will simply translate the style checkpoints into parameters which can be directly used by required bears. These parameters will be passed on to the .coafile of the project.

## Possible alternate way

For 'settings' of a that are boolean, numerical, or have finite number of possible values, an alternate method may be used:  
   1. Make a list of all possible values for a particular setting.
   2. Try out all the values for that setting.
   3. Use the value with the least 'Result' count.
   4. Follow steps 1-3 for all the settings of the bear.

Problem with this method is that, it is slow. Bears would have to be run multiple times on the same set of sample files, and with a large no. of sample files, this would almost be impractical. More reading/research should be done to speed this method up using ML.