# Work Plan

* May 7 - June 7:
   1. Learn basics of ML from Stanford [Machine Learning course](https://www.coursera.org/learn/machine-learning/) on Coursera.
   2. Learn about the state-of=the-art technologies of ML in static code-analysis. Find a proper motivation as to why this project makes sense. This will continue for a
   few months, side-by-side with rest of the schedule.

* June 8 - June 30:
   1. Learn basics of Reinforcement Learning.
   2. Read about Reinforcement Learning bandit problems.
   3. If possible, take a course on RL. [This](https://www.youtube.com/watch?v=2pWv7GOvuf0) could be a valid choice.  

* July 1 - July 10:
   1. Work on a mini-project based on the skills learned so far, possibly create a bot for a board game (Ultimate Tic-Tac-Toe tentatively).  

* July 11 - July 31:
   1. Write SGML for a single code-analysis module(i.e. a bear). This will serve as a prototype to the final implementation.
   2. Write tests to check correctness of implementation.  

**Note**: At this point, the summer vacations will have ended. So, the pace of work has to be slowed down to cope with course work.    

* August 1 - August 10:
   1. Discuss with mentors and figure out the best way to extend SGML to as many languages and bears as possible for a more general SGML.
   2. If needed, make a more detailed design architecture from knowledge gained so far.
   3. Write out the theory behind the implementation so far.  

* August 11 - August 31:
   1. Read about Bag of Words model, Document Term Matrix, PCA/LDA algorithms and other things suggested by mentors, which would help to do further work.   


August 1 - August 31(or more):
   1. Work on the general SFM which is a part of the general SGML.
      * The general SCM will probably be the most complicated part of the project, as it's a hard problem in itself. It might require an extra 2-3 months.  

**Note**: The months starting below are 30-day time periods, counted after the general SCM is completed.    

* Month 1:
   1. Write the general ML which is a part of the general SGML.
   2. Write tests for for the general ML.  

* Month 2:
   1. Integrate the general SFM and ML in a smooth, cohesive manner.
   2. Write the theory behind the general SFM and ML.
   3. If possible, start working on the general FST.  

* Month 3:
   1. Write the general FST which is a part of the general SGML.
   2. Write tests for for the general FST.  

* Month 4:
   1. Work on overall integration of the general SFM, ML, and FST to form the final general SGML.
   2. Write a final set of tests for the general SGML.  

* Month 5:
   1. Tie all loose ends. Finish incomplete work.  

* Month 6:
   1. Evaluate and analyze work done so far.
   2. Finish writing the final paper.  

## Long-term Goals
 
* Year 1:
   1. Find a proper motivation as to why this project makes sense.
   2. Learn about the state-of-the-art technologies of ML in static code-analysis.
   3. Learn the basic and necessary topics under ML and RL.
   4. Write a SGML for a specific *bear*.
   